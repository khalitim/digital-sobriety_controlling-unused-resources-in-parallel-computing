%% EP_benchmark dynamics on the gros cluster
cluster=3;
file_PATH='/home/spirals/Documents/europar-96-artifacts/ep_stairs_three_clusters/signals_1249149509024778752_gros_stairs.yaml.csv';
processAndPlotDataset(file_PATH,cluster);

%%  EP_benchmark dynamics on the dahu cluster
cluster=4;
file_PATH='/home/spirals/Documents/europar-96-artifacts/ep_stairs_three_clusters/signals_-5894624932765925376_dahu_stairs.yaml.csv';
processAndPlotDataset(file_PATH,cluster);

%% EP_benchmark dynamics on the yeti cluster
cluster=6;
file_PATH='/home/spirals/Documents/europar-96-artifacts/ep_stairs_three_clusters/signals_-3463489181351815168_yeti_stairs.yaml.csv';
processAndPlotDataset(file_PATH,cluster);

%% processDataset funtion
% Function to process and plot dataset
function processAndPlotDataset(file_PATH,cluster)
    % Read the dataset
    data = readtable(file_PATH);
    % Convert the 'signal' column to a categorical array
    data.signal = categorical(data.signal);

    % Create a cell array to store the tables
    signalTables = {};

    % Iterate over unique signal types
    uniqueSignals = unique(data.signal);
    for i = 1:numel(uniqueSignals)
        % Filter the data for the current signal type
        currentSignal = uniqueSignals(i);
        filteredData = data(data.signal == currentSignal, :);

        % Create a table for the current signal type
        signalTable = table();
        signalTable.signal = repmat(currentSignal, size(filteredData, 1), 1);
        signalTable.values = filteredData.value;
        signalTable.runtime=filteredData.runtime;

        % Store the signal table in the cell array
        signalTables{i} = signalTable;
        % Plot the values for the current signal type
      % Static model 
       if i == 1
           powercap = filteredData.value
       end

      if i== cluster
% Calculate the sensor mean value every 30 seconds
        timeInterval = 30; % 30 seconds
        startTime = min(filteredData.runtime)+0.5;
        endTime = max(filteredData.runtime);
        timestamps = startTime:timeInterval:endTime;
        meanValues = zeros(length(timestamps)-1, 1);
        for j = 1:numel(timestamps)-1
            startTimeStamp = timestamps(j);
            endTimeStamp = timestamps(j+1);
            meanValues(j) = mean(signalTable.values(signalTable.runtime >= startTimeStamp & signalTable.runtime < endTimeStamp));
        end
           end 
    end
   disp(meanValues(1:5));
   meany=meanValues(1:5);
   % mdl = fitlm(powercap,medians);
   % disp(mdl);
    % Get the coefficient table of the linear regression model
    %coefTable = mdl.Coefficients;
     y1=powercap
    u1=meany
    datarx=iddata(u1,y1)
    %predictedY = predict(mdl, powercap);
% Extract the coefficient for the input variable and the intercept
%inputCoeff = coefTable.Estimate(2);  % Coefficient for the input variable (assuming it is the second row)
%intercept = coefTable.Estimate(1);   % Intercept term (assuming it is the first row)

% Construct the equation string
%eqn = 'y = ';
%eqn = [eqn num2str(inputCoeff) ' * u + ' num2str(intercept)];

% Display the equation
%disp(eqn);
    
    modelorder=[2, 2, 1];
    % Access the model coefficients
    %  plot(u1, y1)

    y1='powecap';
    u1='meany';
    %output_lag = [1 2 3 4];
    %input_lag = [1 2 3 4];
    %lags = {output_lag,input_lag};
    %names = [u, y];
    vars = {'y1','u1'};
     lags = {1:2 0:1};

    L = linearRegressor(vars, lags)
     NL1= arx(datarx, modelorder)


%disp(ARParams);
     %B=NL1.B;     %Estimated regression parameters 
     %C = model.C; % Estimated direct feedthrough parameters
     %D = model.D; % Estimated constant term
     disp(NL1);
      compare(datarx, NL1); 
     set(findall(gca,'Type','line'),'LineWidth',1)
 
     grid on
end
