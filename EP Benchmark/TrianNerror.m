%% Remarks
% make your observation on the behavior of the plot logical (the model
% performance)
% Questions to be asked : what are the next logical steps? 
% again there isnt a unique recipe that we are just following here, model
% selection sends to be a trial and error process 
% we could try to increase the linear model order, or manually set the
% offset term to a pre calculated value
%% EP_benchmark dynamics on the dahu cluster
file_PATH='/home/spirals/Documents/europar-96-artifacts/ep-dahu-identification-20230703/signals_6389457611926497280_dahu_stairs.yaml.csv';
processAndPlotDataset(file_PATH);
%% processDataset funtion
% Function to process and plot dataset
function processAndPlotDataset(file_PATH)
    % Read the dataset
    data = readtable(file_PATH);
    % Convert the 'signal' column to a categorical array
    data.signal = categorical(data.signal);

    % Create a cell array to store the tables
    signalTables = {};

    % Iterate over unique signal types
    uniqueSignals = unique(data.signal);
    for i = 1:numel(uniqueSignals)
        % Filter the data for the current signal type
        currentSignal = uniqueSignals(i);
        filteredData = data(data.signal == currentSignal, :);

        % Create a table for the current signal type
        signalTable = table();
        signalTable.signal = repmat(currentSignal, size(filteredData, 1), 1);
        signalTable.values = filteredData.value;
        signalTable.runtime=filteredData.runtime;

        % Store the signal table in the cell array
        signalTables{i} = signalTable;
        % Plot the values for the current signal type
      % Static model 
       if i == 1 
           powercap = filteredData.value

       end
      if i==4
          window_size = 30;

    % Calculate the number of windows
    num_windows = floor(length(filteredData.value) / window_size);

    % Calculate median for each window
   
    medians = [];
    for j = 1:num_windows-10
    start_index = (j - 1) * window_size + 1;
    end_index = j * window_size;
    values = filteredData.value(start_index:end_index);
    medians = [medians; median(values)]
    end
    mdl = fitlm(powercap,medians);
    disp(mdl);
    % Get the coefficient table of the linear regression model
    coefTable = mdl.Coefficients;
     y1=powercap
    u1=medians
    datarx=iddata(u1,y1)
    predictedY = predict(mdl, powercap);
% Extract the coefficient for the input variable and the intercept
inputCoeff = coefTable.Estimate(2);  % Coefficient for the input variable (assuming it is the second row)
intercept = coefTable.Estimate(1);   % Intercept term (assuming it is the first row)

% Construct the equation string
eqn = 'y = ';
eqn = [eqn num2str(inputCoeff) ' * u + ' num2str(intercept)];

% Display the equation
disp(eqn);
    
    modelorder=[2, 2, 1];
    % Access the model coefficients
      plot(u1, y1)

    y1='powecap';
    u1='medians';
    %output_lag = [1 2 3 4];
    %input_lag = [1 2 3 4];
    %lags = {output_lag,input_lag};
    %names = [u, y];
    vars = {'y1','u1'};
     lags = {1:2 0:1};    L = linearRegressor(vars, lags)
     NL1= nlarx(datarx, L, idLinear);
     % A=NL1.A;    % Estimated AR parameters
     % B=NL1.B;     %Estimated regression parameters 
     % C =NL1.C; % Estimated direct feedthrough parameters
     % D =NL1.D; % Estimated constant term
     disp(NL1);
     compare(datarx, NL1); 
     set(findall(gca,'Type','line'),'LineWidth',1)
 
     grid on
       end 
        hold on;
    end
 
    legend('show');
end