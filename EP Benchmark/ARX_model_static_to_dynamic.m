%% EP_benchmark dynamics on the gros cluster
cluster = 3;

A = [1, 0, 0.9085]; 
B = [0, 0.1599, 0.1191];
 %A = [1, 0.226, -0.7579]; 
 %B = [0, 0.3964, 0];   
file_PATH = '/home/spirals/Documents/europar-96-artifacts/ep_stairs_three_clusters/signals_1249149509024778752_gros_stairs.yaml.csv';
processAndPlotDataset(file_PATH, cluster, A, B);

%% EP_benchmark dynamics on the dahu cluster
cluster = 4;
A=[1 0 0.5517];
B= [0 0.0434 0.3797];
file_PATH = '/home/spirals/Documents/europar-96-artifacts/ep_stairs_three_clusters/signals_-5894624932765925376_dahu_stairs.yaml.csv';
processAndPlotDataset(file_PATH, cluster, A, B);

%% EP_benchmark dynamics on the yeti cluster
cluster = 6;
 A = [1, 0.226, -0.7579]; 
 B = [0, 0.3964, 0]; 
 %A = [1, 0, 0.9085]; 
 %B = [0, 0.1599, 0.1191]; 
file_PATH = '/home/spirals/Documents/europar-96-artifacts/ep_stairs_three_clusters/signals_-3463489181351815168_yeti_stairs.yaml.csv';
processAndPlotDataset(file_PATH, cluster, A, B);

%% processDataset function
% Function to process and plot dataset
function processAndPlotDataset(file_PATH, cluster, A, B)
   
    % Read the dataset
    data = readtable(file_PATH);
    % Convert the 'signal' column to a categorical array
    data.signal = categorical(data.signal);

    % Create a cell array to store the tables
    signalTables = {};

    % Iterate over unique signal types
    uniqueSignals = unique(data.signal);
    for i = 1:numel(uniqueSignals)
        % Filter the data for the current signal type
        currentSignal = uniqueSignals(i);
        filteredData = data(data.signal == currentSignal, :);

        % Create a table for the current signal type
        signalTable = table();
        signalTable.signal = repmat(currentSignal, size(filteredData, 1), 1);
        signalTable.values = filteredData.value;
        signalTable.runtime = filteredData.runtime;

        % Store the signal table in the cell array
        signalTables{i} = signalTable;

        % Plot the values for the current signal type
        
        if i == cluster

% Assuming you have 't' as the array of time values
%n = length(filteredData.runtime); % Number of data points

% Initialize an array to store the filtered output
%filtered_y = filteredData.value;

% Apply the median filter
%for i = 2:n
    % Calculate the filtered value using the median of neighboring differences
 %   filtered_y(i) = median(1 / (filteredData.runtime(i) - filteredData.runtime(i-1)));
%end
%plot(filteredData.runtime, filtered_y)
%pause;



             windowSize = 11; % Window size for median filtering

            filteredValues = medfilt1(filteredData.value, windowSize);
            powercap=40:20:120;
            powercapRepeated1 = repelem(powercap, 31)';
           powercapRepeated2 = repelem(powercap(end), numel(filteredValues)-numel(powercapRepeated1))';
           powercapRepeated= [powercapRepeated1; powercapRepeated2];
           y1 = filteredValues;
            u1 = powercapRepeated;
     
           % Predicted output data for the rest of the data points
         y_predicted_rest = zeros(length(u1),2);
         y_predicted_rest1=zeros(size(u1));
            y_predicted_rest1(1) = filteredValues(1);
              for t = 2:length(u1)
         y_predicted_rest(t,:)=B(2:end) * u1(t)  +  A(2:end) * y_predicted_rest(t - 1);
         y_predicted_rest1(t)=(y_predicted_rest(t,1)+y_predicted_rest(t,2))/3.1;
         %3.1, 2, 3
              end
          % y_predicted_rest contains the predicted output values for the rest of the data points
                plot(filteredData.runtime,y_predicted_rest1);
                hold on 
                plot(filteredData.runtime, filteredData.value)
                hold off
                xlabel('runtime [s]')
                ylabel('progress [Hz]')
        end
    end
end
