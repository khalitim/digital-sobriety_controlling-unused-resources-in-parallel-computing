%% EP_benchmark dynamics on the dahu cluster
cluster=4;
file_PATH='C:\Users\Quds\Downloads\ALL_Files\europar-96-artifacts\ep_stairs_three_clusters\signals_-5894624932765925376_dahu_stairs.yaml.csv';
y_raw_dahu=processAndPlotDataset(file_PATH,cluster);
%% %% EP_benchmark dynamics on the Gros cluster
cluster=3;
file_PATH='C:\Users\Quds\Downloads\ALL_Files\europar-96-artifacts\ep_stairs_three_clusters\signals_1249149509024778752_gros_stairs.yaml.csv';
y_raw_gros=processAndPlotDataset(file_PATH,cluster);
%% EP_benchmark dynamics on the Yeti cluster
cluster=6; 
file_PATH='C:\Users\Quds\Downloads\ALL_Files\europar-96-artifacts\ep_stairs_three_clusters\signals_-3463489181351815168_yeti_stairs.yaml.csv';
y_raw_yeti=processAndPlotDataset(file_PATH,cluster);
%% processDataset funtion
% Function to process and plot dataset
function y_raw=processAndPlotDataset(file_PATH,cluster)
    % Read the dataset
    data = readtable(file_PATH);

    % Convert the 'signal' column to a categorical array
    data.signal = categorical(data.signal);

    % Create a cell array to store the tables
    signalTables = {};

    % Iterate over unique signal types
    uniqueSignals = unique(data.signal);
    for i = 1:numel(uniqueSignals)
        % Filter the data for the current signal type
        currentSignal = uniqueSignals(i);
        filteredData = data(data.signal == currentSignal, :);

        % Create a table for the current signal type
        signalTable = table();
        signalTable.signal = repmat(currentSignal, size(filteredData, 1), 1);
        signalTable.values = filteredData.value;
        signalTable.runtime=filteredData.runtime;

        % Store the signal table in the cell array
        signalTables{i} = signalTable;
       
       % Plot the values for the current signal type
         hold on;
          xlabel('Time [s]');
         ylabel('Power [W]');
       % Set the legend
       legend('show');   
       grid on;
       
        if i == 1 
       
            subplot(2,1,2);
             stairs(filteredData.runtime, filteredData.value, 'k', 'DisplayName', char(currentSignal));
          
             ylim([30,125]);  xlim([0,150]);
        elseif i==2
            
              plot(filteredData.runtime, filteredData.value ,'r*','DisplayName', char(currentSignal));
        elseif i==cluster
            
            y_raw=filteredData.value;
            % figure;
           subplot(2,1,1);
            scatter(filteredData.runtime, filteredData.value ,5, 'filled', 'MarkerEdgeColor','k', 'MarkerFaceColor','k', 'Marker','o', 'DisplayName', char(currentSignal));
        else 
            plot(filteredData.runtime, filteredData.value , 'r*','DisplayName', char(currentSignal));

        end
       
    end
       xlim([0,150]);
    hold on;
          xlabel('Time [s]');
         ylabel('Progress [Hz]');
       % Set the legend
       legend('show');   
       grid on;
 
     %Access the tables for each signal type
   %  for i = 1:numel(signalTables)
    %     fprintf('Signal Type: %s\n', char(signalTables{i}.signal(1)));
    %     disp(signalTables{i});
    %     fprintf('\n');
   %  end
    end