%% EP_benchmark dynamics on the dahu cluster
file_PATH='/home/spirals/Documents/europar-96-artifacts/ep_stairs_three_clusters/signals_-5894624932765925376_dahu_stairs.yaml.csv';
processAndPlotDataset(file_PATH);
%% %% EP_benchmark dynamics on the Gros cluster
file_PATH='/home/spirals/Documents/europar-96-artifacts/ep_stairs_three_clusters/signals_1249149509024778752_gros_stairs.yaml.csv';
processAndPlotDataset(file_PATH);
%% EP_benchmark dynamics on the Yeti cluster
file_PATH='/home/spirals/Documents/europar-96-artifacts/ep_stairs_three_clusters/signals_-3463489181351815168_yeti_stairs.yaml.csv';
processAndPlotDataset(file_PATH);
%% processDataset funtion
% Function to process and plot dataset
function processAndPlotDataset(file_PATH)
    % Read the dataset
    data = readtable(file_PATH);

    % Convert the 'signal' column to a categorical array
    data.signal = categorical(data.signal);

    % Create a cell array to store the tables
    signalTables = {};

    % Iterate over unique signal types
    uniqueSignals = unique(data.signal);
    for i = 1:numel(uniqueSignals)
        % Filter the data for the current signal type
        currentSignal = uniqueSignals(i);
        filteredData = data(data.signal == currentSignal, :);

        % Create a table for the current signal type
        signalTable = table();
        signalTable.signal = repmat(currentSignal, size(filteredData, 1), 1);
        signalTable.values = filteredData.value;
        signalTable.runtime = filteredData.runtime;

    % Calculate the sensor mean value every 30 seconds
        timeInterval = 30; % 30 seconds
        startTime = min(filteredData.runtime);
        endTime = max(filteredData.runtime);
        timestamps = startTime:timeInterval:endTime;
        meanValues = zeros(length(timestamps)-1, 1);
        for j = 1:numel(timestamps)-1
            startTimeStamp = timestamps(j);
            endTimeStamp = timestamps(j+1);
            meanValues(j) = mean(signalTable.values(signalTable.runtime >= startTimeStamp & signalTable.runtime < endTimeStamp));
        end
        
        % Repeat the mean values to match the height of the table
        numRows = size(signalTable, 1);
        numMeanValues = numel(meanValues);
        if numMeanValues < numRows
            meanValues = [meanValues; repmat(meanValues(end), numRows - numMeanValues, 1)];
        elseif numMeanValues > numRows
            meanValues = meanValues(1:numRows);
        end
        
        % Create a column for the mean values in the signal table
        signalTable.meanValue = meanValues;

        % Store the signal table in the cell array
        signalTables{i} = signalTable;
        % Plot the values for the current signal type
        if i == 1
            stairs(filteredData.runtime, filteredData.value, 'DisplayName', char(currentSignal));
        elseif i==2 
            plot(filteredData.runtime, filteredData.value ,'o','DisplayName', char(currentSignal));
        elseif i==4

            plot(filteredData.runtime, filteredData.value , 'o', 'DisplayName', char(currentSignal));
        else 
            plot(filteredData.runtime, filteredData.value , 'o','DisplayName', char(currentSignal));

        end
        hold on;
    end
      xlabel('Time in seconds');
      ylabel('progress [Hz]');
      title('EP Benchmark on cluster');
    % Set the legend
    legend('show');

    % Access the tables for each signal type
    for i = 1:numel(signalTables)
        fprintf('Signal Type: %s\n', char(signalTables{i}.signal(1)));
        disp(signalTables{i});
        fprintf('\n');
    end
    end