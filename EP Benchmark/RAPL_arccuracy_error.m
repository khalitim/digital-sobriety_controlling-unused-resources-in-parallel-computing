     
%% EP_benchmark dynamics on the gros cluster
cluster=3;
file_PATH='C:\Users\Quds\Downloads\ALL_Files\europar-96-artifacts\ep_stairs_three_clusters\signals_1249149509024778752_gros_stairs.yaml.csv';
     processAndPlotDataset(file_PATH); 

%%  EP_benchmark dynamics on the dahu cluster
cluster=4;
file_PATH='C:\Users\Quds\Downloads\ALL_Files\europar-96-artifacts\ep_stairs_three_clusters\signals_-5894624932765925376_dahu_stairs.yaml.csv';
processAndPlotDataset(file_PATH);

%% EP_benchmark dynamics on the yeti cluster
cluster=6;
file_PATH='C:\Users\Quds\Downloads\ALL_Files\europar-96-artifacts\ep_stairs_three_clusters\signals_-3463489181351815168_yeti_stairs.yaml.csv';
processAndPlotDataset(file_PATH);
%% processDataset funtion
% Function to process and plot dataset
function  processAndPlotDataset(file_PATH)
    % Read the dataset
    data = readtable(file_PATH);
    % Convert the 'signal' column to a categorical array
    data.signal = categorical(data.signal);

    % Create a cell array to store the tables
    signalTables = {};

    % Iterate over unique signal types
    uniqueSignals = unique(data.signal);
    for i = 1:numel(uniqueSignals)
        % Filter the data for the current signal type
        currentSignal = uniqueSignals(i);
        filteredData = data(data.signal == currentSignal, :);

        % Create a table for the current signal type
        signalTable = table();
        signalTable.signal = repmat(currentSignal, size(filteredData, 1), 1);
        signalTable.values = filteredData.value;
        signalTable.runtime=filteredData.runtime;

        % Store the signal table in the cell array
        signalTables{i} = signalTable;
        % Plot the values for the current signal type
      % Static model 
       if i == 1
           powercap = filteredData.value;
          
            powercapRepeated1 = [repelem(powercap, 31)];% it was 31
            powercapRepeated2 = [repelem(powercap(end), 672-numel(powercapRepeated1))]';
            powercapRepeated= [powercapRepeated1; powercapRepeated2];
            u1 = powercapRepeated;
            numel(u1)
       end

      if i== 2 
      RAPL_Sensor = filteredData.value; 
      numel(RAPL_Sensor)
      Sensor_accuracy = powercapRepeated - RAPL_Sensor;
      plot(filteredData.runtime,Sensor_accuracy,'o');
      grid on;
      [powercapRepeated, RAPL_Sensor];
      end
    end
end 


