format long
% Given data
x_data = [40, 60, 80, 100, 120];
y_data = [38, 56, 69, 84, 100];

% Define the power-law function to fit
power_law = @(params, x) params(1) * x.^params(2);

% Perform the curve fit
initial_guess = [1, 1];  % Initial parameter guesses
fit_params = lsqcurvefit(power_law, initial_guess, x_data, y_data);

% Extract the fitting parameters
a_fit = fit_params(1);
b_fit = fit_params(2);

% Generate fitted data using the fitted parameters
x_fit = linspace(min(x_data), max(x_data), 100);
y_fit = power_law(fit_params, x_fit);

% Plot the original data and the fitted curve
scatter(x_data, y_data, 'o', 'DisplayName', 'Data');
hold on;
plot(x_fit, y_fit, 'r', 'DisplayName', 'Fitted Curve');
xlabel('Input');
ylabel('Output');
title('Nonlinear Fit using Power-Law Function');
legend('Location', 'northwest');
hold off;

% Display the fitted parameters
fprintf('Fitted parameter a: %f\n', a_fit);
fprintf('Fitted parameter b: %f\n', b_fit);
