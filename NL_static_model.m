%% useful commands 
%{
to save your file : save('loading_data.mat')
%}
%% Document formatting 
format short;
%% data exportation----------------------------------------------------------------------------- 
% those are raw data files 
%Time series data frames---------------------------------------------------
file_PATH='europar-96-artifacts/tools/analysis/refactor/CSV files/campaign_timeseries_df.csv';
datag=readtable(file_PATH) 

%meta data ----------------------------------------------------------------
file_PATH='europar-96-artifacts/tools/analysis/refactor/CSV files/campaign_metadata_df';
datag=readtable(file_PATH)

%input signal u(powecap signal data........................................................

file_PATH='europar-96-artifacts/tools/analysis/refactor/CSV files/dump_pubMeasurements.csv';
datag=readtable(file_PATH)

%output signal y data........................................................

file_PATH='europar-96-artifacts/tools/analysis/refactor/CSV files/dump_pubProgress.csv';
datag=readtable(file_PATH)
%% installing Yaml4matlab toolbox 
%{
 fisrt we need to import the yaml files for the identification inputs: 
 we need to verify first wether the YAML4MATLAB toolbox is installed. 

toolboxname = 'YAML4MATLAB';
itexists = false; 
installedToolboxes =ver;
for i=1:numel(installedToolboxes)
    if strcmp(installedToolboxes(i).Name,toolboxname)
        itexists = true;
        break;
    end
end
if itexists 
    disp(['the ',toolboxname ,' Exists']);
else
    disp(['the ', toolboxname ,' Doesnt exists']);
end
%{
 since it doesnt exist we can download it from github (check File Exchange
 in Mathworks)
type in the CW: addpath('C:\Users\Quds\Desktop\Internship\yamltoolbox')
%}
%}

%% -------------study of the static case since the figures for static are unaccessible for the moment-------------------------------------

syms u
u = [45:5:120];
beta = {'gros', 0.83; 'chifflot', 1.03; 'dahu', 0.94};
delta = {'gros', 7.07; 'chifflot', 4.04; 'dahu', 0.17};
alpha = {'gros', 0.047; 'chifflot', 0.028; 'dahu', 0.032};
gamma = {'gros', 28.5; 'chifflot', 37.04; 'dahu', 34.8};
K = {'gros', 25.6; 'chifflot', 42.82; 'dahu', 42.4};

% Asymptotic model of the system 
Clusters = {'gros_model', 'chifflot_model', 'dahu_model'};
y = cell(1, numel(Clusters));

for i = 1:numel(Clusters)
    y{i} = K{i, 2} * (1 - exp(-alpha{i, 2} * (beta{i, 2} * u - gamma{i, 2} + delta{i, 2})));
end

% Plotting
figure;
hold on;
for i = 1:numel(Clusters)
    plot(u, y{i}, 'DisplayName', Clusters{i});
end
hold off;
legend('Location', 'best');
xlabel('Output');
ylabel('Input');
title('System Response for Each Cluster'); 