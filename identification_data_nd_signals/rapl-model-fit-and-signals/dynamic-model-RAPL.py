import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d
from scipy.optimize import minimize
import argparse
import os

def load_data(file_path):
    # Load the data using pandas
    data = pd.read_csv(file_path)
    return data

# Discrete first-order system model
def discrete_first_order_system(params, u, y):
    a, b = params
    y_model = np.zeros_like(y)
    y_model[0] = y[0]  # Initial condition: assuming y[0] is known

    # Simulate the discrete system
    for k in range(1, len(y)):
        y_model[k] = a * y_model[k - 1] + b * u[k - 1]
    
    return y_model

# Cost function for optimization (mean squared error)
def cost_function(params, u, y):
    y_model = discrete_first_order_system(params, u, y)
    return np.sum((y - y_model) ** 2)

def fit_discrete_first_order_model(actuator_values, sensor_values):
    # Initial guess for parameters a and b
    initial_params = [0.9, 0.1]

    # Use optimization to fit the model to the data
    result = minimize(cost_function, initial_params, args=(actuator_values, sensor_values), method='BFGS')
    
    # Extract standard error (uncertainty) from the covariance matrix
    cov_matrix = result.hess_inv  # Hessian inverse gives the covariance matrix
    param_uncertainties = np.sqrt(np.diag(cov_matrix))  # Standard errors are the square root of the diagonal

    return result.x, param_uncertainties  # Return the estimated parameters and uncertainties

def process_file(file_path, num_sensors):
    # Extract cluster name from the file
    cluster_name = os.path.basename(file_path).split('_')[-1].split('.')[0]

    # Load data
    data = load_data(file_path)

    # Filter actuator data
    actuator_data = data[data['signal'] == 'actuator-pcap']

    # Extract time and values for actuator
    time_actuator = actuator_data['runtime'].values
    actuator_values = actuator_data['value'].values

    # Store sensor data, model parameters, and uncertainties
    sensor_data = []
    model_params = []
    uncertainties = []

    # Loop through the number of sensors specified by the user
    for sensor_index in range(num_sensors):
        sensor_data_pkg = data[data['signal'] == f'sensor-pkg-{sensor_index}']

        # Extract time and values for the current sensor
        time_sensor_pkg = sensor_data_pkg['runtime'].values
        sensor_values_pkg = sensor_data_pkg['value'].values

        # Map the actuator step inputs to the sensor time points by forward-filling
        actuator_values_step_filled_pkg = np.zeros_like(time_sensor_pkg)

        for i, t in enumerate(time_sensor_pkg):
            idx = np.searchsorted(time_actuator, t, side='right') - 1
            if idx >= 0:
                actuator_values_step_filled_pkg[i] = actuator_values[idx]
            else:
                actuator_values_step_filled_pkg[i] = actuator_values[0]

        # Fit the discrete-time model for the current sensor output
        model_param, uncertainty = fit_discrete_first_order_model(actuator_values_step_filled_pkg, sensor_values_pkg)
        model_params.append(model_param)
        uncertainties.append(uncertainty)

        # Store sensor data for plotting
        sensor_data.append((time_sensor_pkg, sensor_values_pkg))

        print(f"Fitted parameters for sensor-pkg-{sensor_index}: a = {model_param[0]:.3f} ± {uncertainty[0]:.3f}, b = {model_param[1]:.3f} ± {uncertainty[1]:.3f}")

    # Calculate model uncertainty between sensor packages
    if len(model_params) > 1:
        for i in range(len(model_params) - 1):
            a_diff = model_params[i][0] - model_params[i + 1][0]
            b_diff = model_params[i][1] - model_params[i + 1][1]
            a_combined_uncertainty = np.sqrt(uncertainties[i][0] ** 2 + uncertainties[i + 1][0] ** 2)
            b_combined_uncertainty = np.sqrt(uncertainties[i][1] ** 2 + uncertainties[i + 1][1] ** 2)

            print(f"Model uncertainty between sensor-pkg-{i} and sensor-pkg-{i + 1}:")
            print(f"  a difference: {a_diff:.3f} ± {a_combined_uncertainty:.3f}")
            print(f"  b difference: {b_diff:.3f} ± {b_combined_uncertainty:.3f}")

    # Print the transfer functions for each sensor
    for index, (params) in enumerate(model_params):
        transfer_function = f"H_pkg-{index}(z) = {params[1]:.3f} / (1 - {params[0]:.3f}z^(-1))"
        print(f"\nTransfer Function for sensor-pkg-{index}:")
        print(transfer_function)

    # Simulate the model with the fitted parameters for plotting
    plt.figure(figsize=(10, 6))

    # Plot actuator values only once
    plt.plot(time_sensor_pkg, actuator_values_step_filled_pkg, label="Actuator (input)", linestyle="--")

    for index in range(num_sensors):
        time_sensor, sensor_values = sensor_data[index]
        params = model_params[index]
        
        # Simulate the sensor model using the actuator values
        y_model_pkg = discrete_first_order_system(params, actuator_values_step_filled_pkg, sensor_values)
        
        # Plot sensor values and model output
        plt.plot(time_sensor, sensor_values, label=f"Sensor-pkg-{index} (Output)", linestyle="-")
        plt.plot(time_sensor, y_model_pkg, label=f"Model sensor-pkg-{index}: a={params[0]:.3f}, b={params[1]:.3f}", linestyle=":")

    plt.title(f"Discrete-Time Model Fit for Cluster {cluster_name}")
    plt.xlabel("Time")
    plt.ylabel("Values")
    plt.legend()
    plt.grid(True)

    # Save the plot
    plot_filename = f"discrete_model_fit_cluster_{cluster_name}.png"
    plt.savefig(plot_filename)
    plt.show()

def main():
    # Argument parser for terminal input
    parser = argparse.ArgumentParser(description="Fit first-order dynamic model to data.")
    parser.add_argument("file", type=str, help="Path to the CSV file (e.g., europar-96_fig-3a-grous.csv)")
    parser.add_argument("num_sensors", type=int, help="Number of sensor packages (e.g., 2 for sensor-pkg-0 and sensor-pkg-1)")
    args = parser.parse_args()

    # Process the input file with specified number of sensors
    process_file(args.file, args.num_sensors)

if __name__ == "__main__":
    main()
