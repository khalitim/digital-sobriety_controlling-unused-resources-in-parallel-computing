import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy.interpolate import interp1d
from scipy.optimize import minimize
import argparse
import os

# First-order system function (to be used for curve fitting)
def first_order_system(t, K, tau):
    return K * (1 - np.exp(-t / tau))

def load_data(file_path):
    # Load the data using pandas
    data = pd.read_csv(file_path)
    return data

def fit_first_order_model(time, sensor_data):
    # Fit a first-order dynamic model to the data
    popt, _ = curve_fit(first_order_system, time, sensor_data, p0=[1, 1])  # Initial guess for K and tau
    return popt

def plot_data_and_model(time, actuator_data, sensor_data, sensor_data1, model_params, cluster_name):
    # Generate model prediction using fitted parameters
    K, tau = model_params
    model_prediction = first_order_system(time, K, tau)

    # Plot the actual data and the model
    plt.figure(figsize=(10, 6))
    plt.plot(time, actuator_data, label="Actuator (Input)", linestyle="--")
    plt.plot(time, sensor_data, label="Sensor (Output)", linestyle="-")
    plt.plot(time, sensor_data1, label="Sensor1 (Output)", linestyle="-")
    plt.plot(time, model_prediction, label=f"Model: K={K:.3f}, tau={tau:.3f}", linestyle=":")

    plt.title(f"First Order Model Fit for Cluster {cluster_name}")
    plt.xlabel("Time")
    plt.ylabel("Values")
    plt.legend()
    plt.grid(True)

    # Save the plot
    plot_filename = f"model_fit_cluster_{cluster_name}.png"
    plt.savefig(plot_filename)
    plt.show()

    return plot_filename

def save_model_parameters(cluster_name, model_params):
    # Save the model parameters to a CSV file
    df = pd.DataFrame([model_params], columns=['K', 'tau'])
    filename = f"model_params_cluster_{cluster_name}.csv"
    df.to_csv(filename, index=False)
    print(f"Model parameters saved to {filename}")

# Discrete first-order system model
def discrete_first_order_system(params, u, y):
    a, b = params
    y_model = np.zeros_like(y)
    y_model[0] = y[0]  # Initial condition: assuming y[0] is known

    # Simulate the discrete system
    for k in range(1, len(y)):
        y_model[k] = a * y_model[k - 1] + b * u[k - 1]
    
    return y_model

# Cost function for optimization (mean squared error)
def cost_function(params, u, y):
    y_model = discrete_first_order_system(params, u, y)
    return np.sum((y - y_model) ** 2)

def fit_discrete_first_order_model(actuator_values, sensor_values):
    # Initial guess for parameters a and b
    initial_params = [0.9, 0.1]

    # Use optimization to fit the model to the data
    result = minimize(cost_function, initial_params, args=(actuator_values, sensor_values), method='BFGS')
    
    # Extract standard error (uncertainty) from the covariance matrix
    cov_matrix = result.hess_inv  # Hessian inverse gives the covariance matrix
    param_uncertainties = np.sqrt(np.diag(cov_matrix))  # Standard errors are the square root of the diagonal

    return result.x, param_uncertainties  # Return the estimated parameters and uncertainties

def process_file(file_path):
    # Extract cluster name from the file
    cluster_name = os.path.basename(file_path).split('_')[-1].split('.')[0]

    # Load data
    data = load_data(file_path)

    # Filter actuator and sensor data
    actuator_data = data[data['signal'] == 'actuator-pcap']
    sensor_data_pkg = data[data['signal'] == 'sensor-pkg-0']
    sensor_data_pkg_1 = data[data['signal'] == 'sensor-pkg-1']

    # Extract time and values for actuator
    time_actuator = actuator_data['runtime'].values
    actuator_values = actuator_data['value'].values

    # Extract time and values for both sensors
    time_sensor_pkg = sensor_data_pkg['runtime'].values
    sensor_values_pkg = sensor_data_pkg['value'].values

    time_sensor_pkg_1 = sensor_data_pkg_1['runtime'].values
    sensor_values_pkg_1 = sensor_data_pkg_1['value'].values

    # Map the actuator step inputs to the sensor time points by forward-filling
    actuator_values_step_filled_pkg = np.zeros_like(time_sensor_pkg)
    actuator_values_step_filled_pkg_1 = np.zeros_like(time_sensor_pkg_1)

    for i, t in enumerate(time_sensor_pkg):
        idx = np.searchsorted(time_actuator, t, side='right') - 1
        if idx >= 0:
            actuator_values_step_filled_pkg[i] = actuator_values[idx]
        else:
            actuator_values_step_filled_pkg[i] = actuator_values[0]

    for i, t in enumerate(time_sensor_pkg_1):
        idx = np.searchsorted(time_actuator, t, side='right') - 1
        if idx >= 0:
            actuator_values_step_filled_pkg_1[i] = actuator_values[idx]
        else:
            actuator_values_step_filled_pkg_1[i] = actuator_values[0]

    # Fit the discrete-time model for both sensor outputs
    model_params_pkg, uncertainties_pkg = fit_discrete_first_order_model(actuator_values_step_filled_pkg, sensor_values_pkg)
    model_params_pkg_1, uncertainties_pkg_1 = fit_discrete_first_order_model(actuator_values_step_filled_pkg_1, sensor_values_pkg_1)

    print(f"Fitted parameters for sensor-pkg: a = {model_params_pkg[0]:.3f} ± {uncertainties_pkg[0]:.3f}, b = {model_params_pkg[1]:.3f} ± {uncertainties_pkg[1]:.3f}")
    print(f"Fitted parameters for sensor-pkg-1: a = {model_params_pkg_1[0]:.3f} ± {uncertainties_pkg_1[0]:.3f}, b = {model_params_pkg_1[1]:.3f} ± {uncertainties_pkg_1[1]:.3f}")

    # Calculate model uncertainty between the two sensors
    a_diff = model_params_pkg[0] - model_params_pkg_1[0]
    b_diff = model_params_pkg[1] - model_params_pkg_1[1]

    # Calculate the combined uncertainty
    a_combined_uncertainty = np.sqrt(uncertainties_pkg[0]**2 + uncertainties_pkg_1[0]**2)
    b_combined_uncertainty = np.sqrt(uncertainties_pkg[1]**2 + uncertainties_pkg_1[1]**2)

    print(f"Model uncertainty between sensor-pkg and sensor-pkg-1:")
    print(f"  a difference: {a_diff:.3f} ± {a_combined_uncertainty:.3f}")
    print(f"  b difference: {b_diff:.3f} ± {b_combined_uncertainty:.3f}")

    # Simulate the model with the fitted parameters
    y_model_pkg = discrete_first_order_system(model_params_pkg, actuator_values_step_filled_pkg, sensor_values_pkg)
    y_model_pkg_1 = discrete_first_order_system(model_params_pkg_1, actuator_values_step_filled_pkg_1, sensor_values_pkg_1)

    # Plot the actual data and the model for both sensors
    plt.figure(figsize=(10, 6))
    plt.plot(time_sensor_pkg, actuator_values_step_filled_pkg, label="Actuator (Input)", linestyle="--")
    plt.plot(time_sensor_pkg, sensor_values_pkg, label="Sensor-pkg (Output)", linestyle="-")
    plt.plot(time_sensor_pkg, y_model_pkg, label=f"Model sensor-pkg: a={model_params_pkg[0]:.3f} ± {uncertainties_pkg[0]:.3f}, b={model_params_pkg[1]:.3f} ± {uncertainties_pkg[1]:.3f}", linestyle=":")

    plt.plot(time_sensor_pkg_1, sensor_values_pkg_1, label="Sensor-pkg-1 (Output)", linestyle="-.")
    plt.plot(time_sensor_pkg_1, y_model_pkg_1, label=f"Model sensor-pkg-1: a={model_params_pkg_1[0]:.3f} ± {uncertainties_pkg_1[0]:.3f}, b={model_params_pkg_1[1]:.3f} ± {uncertainties_pkg_1[1]:.3f}", linestyle=":")

    plt.title(f"Discrete-Time Model Fit for Cluster {cluster_name}")
    plt.xlabel("Time")
    plt.ylabel("Values")
    plt.legend()
    plt.grid(True)

    # Save the plot
    plot_filename = f"discrete_model_fit_cluster_{cluster_name}.png"
    plt.savefig(plot_filename)
    plt.show()

    # Save model parameters and uncertainties to CSV
    save_model_parameters(cluster_name, model_params_pkg, uncertainties_pkg, model_params_pkg_1, uncertainties_pkg_1)

def save_model_parameters(cluster_name, model_params_pkg, uncertainties_pkg, model_params_pkg_1, uncertainties_pkg_1, delta_a, delta_b):
    # Save the model parameters and uncertainties to a CSV file
    df = pd.DataFrame({
        'Sensor': ['sensor-pkg', 'sensor-pkg-1'],
        'a': [model_params_pkg[0], model_params_pkg_1[0]],
        'b': [model_params_pkg[1], model_params_pkg_1[1]],
        'a_uncertainty': [uncertainties_pkg[0], uncertainties_pkg_1[0]],
        'b_uncertainty': [uncertainties_pkg[1], uncertainties_pkg_1[1]],
        'delta_a': [delta_a, np.nan],  # Set delta_a for the first sensor, NaN for the second
        'delta_b': [delta_b, np.nan]   # Set delta_b for the first sensor, NaN for the second
    })
    filename = f"discrete_model_params_cluster_{cluster_name}.csv"
    df.to_csv(filename, index=False)
    print(f"Model parameters saved to {filename}")

def process_file(file_path):
    # Extract cluster name from the file
    cluster_name = os.path.basename(file_path).split('_')[-1].split('.')[0]

    # Load data
    data = load_data(file_path)

    # Filter actuator and sensor data
    actuator_data = data[data['signal'] == 'actuator-pcap']
    sensor_data_pkg = data[data['signal'] == 'sensor-pkg-0']
    sensor_data_pkg_1 = data[data['signal'] == 'sensor-pkg-1']

    # Extract time and values for actuator
    time_actuator = actuator_data['runtime'].values
    actuator_values = actuator_data['value'].values

    # Extract time and values for both sensors
    time_sensor_pkg = sensor_data_pkg['runtime'].values
    sensor_values_pkg = sensor_data_pkg['value'].values

    time_sensor_pkg_1 = sensor_data_pkg_1['runtime'].values
    sensor_values_pkg_1 = sensor_data_pkg_1['value'].values

    # Map the actuator step inputs to the sensor time points by forward-filling
    actuator_values_step_filled_pkg = np.zeros_like(time_sensor_pkg)
    actuator_values_step_filled_pkg_1 = np.zeros_like(time_sensor_pkg_1)

    for i, t in enumerate(time_sensor_pkg):
        idx = np.searchsorted(time_actuator, t, side='right') - 1
        if idx >= 0:
            actuator_values_step_filled_pkg[i] = actuator_values[idx]
        else:
            actuator_values_step_filled_pkg[i] = actuator_values[0]

    for i, t in enumerate(time_sensor_pkg_1):
        idx = np.searchsorted(time_actuator, t, side='right') - 1
        if idx >= 0:
            actuator_values_step_filled_pkg_1[i] = actuator_values[idx]
        else:
            actuator_values_step_filled_pkg_1[i] = actuator_values[0]

    # Fit the discrete-time model for both sensor outputs
    model_params_pkg, uncertainties_pkg = fit_discrete_first_order_model(actuator_values_step_filled_pkg, sensor_values_pkg)
    model_params_pkg_1, uncertainties_pkg_1 = fit_discrete_first_order_model(actuator_values_step_filled_pkg_1, sensor_values_pkg_1)

    print(f"Fitted parameters for sensor-pkg: a = {model_params_pkg[0]:.3f} ± {uncertainties_pkg[0]:.3f}, b = {model_params_pkg[1]:.3f} ± {uncertainties_pkg[1]:.3f}")
    print(f"Fitted parameters for sensor-pkg-1: a = {model_params_pkg_1[0]:.3f} ± {uncertainties_pkg_1[0]:.3f}, b = {model_params_pkg_1[1]:.3f} ± {uncertainties_pkg_1[1]:.3f}")

    # Calculate model uncertainty between the two sensors
    a_diff = model_params_pkg[0] - model_params_pkg_1[0]
    b_diff = model_params_pkg[1] - model_params_pkg_1[1]

    # Calculate the combined uncertainty
    a_combined_uncertainty = np.sqrt(uncertainties_pkg[0]**2 + uncertainties_pkg_1[0]**2)
    b_combined_uncertainty = np.sqrt(uncertainties_pkg[1]**2 + uncertainties_pkg_1[1]**2)

    print(f"Model uncertainty between sensor-pkg and sensor-pkg-1:")
    print(f"  a difference: {a_diff:.3f} ± {a_combined_uncertainty:.3f}")
    print(f"  b difference: {b_diff:.3f} ± {b_combined_uncertainty:.3f}")

    # Simulate the model with the fitted parameters
    y_model_pkg = discrete_first_order_system(model_params_pkg, actuator_values_step_filled_pkg, sensor_values_pkg)
    y_model_pkg_1 = discrete_first_order_system(model_params_pkg_1, actuator_values_step_filled_pkg_1, sensor_values_pkg_1)

    # Plot the actual data and the model for both sensors
    plt.figure(figsize=(10, 6))
    plt.plot(time_sensor_pkg, actuator_values_step_filled_pkg, label="Actuator (Input)", linestyle="--")
    plt.plot(time_sensor_pkg, sensor_values_pkg, label="Sensor-pkg (Output)", linestyle="-")
    plt.plot(time_sensor_pkg, y_model_pkg, label=f"Model sensor-pkg: a={model_params_pkg[0]:.3f} ± {uncertainties_pkg[0]:.3f}, b={model_params_pkg[1]:.3f} ± {uncertainties_pkg[1]:.3f}", linestyle=":")

    plt.plot(time_sensor_pkg_1, sensor_values_pkg_1, label="Sensor-pkg-1 (Output)", linestyle="-.")
    plt.plot(time_sensor_pkg_1, y_model_pkg_1, label=f"Model sensor-pkg-1: a={model_params_pkg_1[0]:.3f} ± {uncertainties_pkg_1[0]:.3f}, b={model_params_pkg_1[1]:.3f} ± {uncertainties_pkg_1[1]:.3f}", linestyle=":")

    plt.title(f"Discrete-Time Model Fit for Cluster {cluster_name}")
    plt.xlabel("Time")
    plt.ylabel("Values")
    plt.legend()
    plt.grid(True)

    # Save the plot
    plot_filename = f"discrete_model_fit_cluster_{cluster_name}.png"
    plt.savefig(plot_filename)
    plt.show()

    # Save model parameters and uncertainties to CSV
    save_model_parameters(cluster_name, model_params_pkg, uncertainties_pkg, model_params_pkg_1, uncertainties_pkg_1, a_diff, b_diff)

def main():
    # Argument parser for terminal input
    parser = argparse.ArgumentParser(description="Fit first-order dynamic model to data.")
    parser.add_argument("file", type=str, help="Path to the CSV file (e.g., europar-96_fig-3a-grous.csv)")
    args = parser.parse_args()

    # Process the input file
    process_file(args.file)

if __name__ == "__main__":
    main()
