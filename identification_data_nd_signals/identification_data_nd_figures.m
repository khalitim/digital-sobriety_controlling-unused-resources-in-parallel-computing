% Function to process dataset and create plots
processDataset = @(file_PATH) processAndPlotDataset(file_PATH);
%% gros_cluster_open loop_signal
% Dataset A
file_PATH = '/home/spirals/europar-96-artifacts/europar-96_fig-3/europar-96_fig-3a.csv';
processDataset(file_PATH);
%% dahu_cluster_open loop_signal
% Dataset B
file_PATH = '/home/spirals/europar-96-artifacts/europar-96_fig-3/europar-96_fig-3b.csv';
processDataset(file_PATH);
%% _cluster_open loop_signal
% Dataset C
file_PATH = '/home/spirals/europar-96-artifacts/europar-96_fig-3/europar-96_fig-3c.csv';
processDataset(file_PATH);
%% processDataset funtion
% Function to process and plot dataset
function processAndPlotDataset(file_PATH)
    % Read the dataset
    data = readtable(file_PATH);

    % Convert the 'signal' column to a categorical array
    data.signal = categorical(data.signal);

    % Create a cell array to store the tables
    signalTables = {};

    % Iterate over unique signal types
    uniqueSignals = unique(data.signal);
    for i = 1:numel(uniqueSignals)
        % Filter the data for the current signal type
        currentSignal = uniqueSignals(i);
        filteredData = data(data.signal == currentSignal, :);

        % Create a table for the current signal type
        signalTable = table();
        signalTable.signal = repmat(currentSignal, size(filteredData, 1), 1);
        signalTable.values = filteredData.value;

        % Store the signal table in the cell array
        signalTables{i} = signalTable;

        % Plot the values for the current signal type
        if i == 1
            stairs(filteredData.runtime, filteredData.value, 'DisplayName', char(currentSignal));
        else
            plot(filteredData.runtime, filteredData.value,'o' ,'DisplayName', char(currentSignal));
        end
        hold on;
    end

    % Set the legend
    legend('show');

    % Access the tables for each signal type
    for i = 1:numel(signalTables)
        fprintf('Signal Type: %s\n', char(signalTables{i}.signal(1)));
        disp(signalTables{i});
        fprintf('\n');
    end
end

