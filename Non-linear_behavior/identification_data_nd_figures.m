%% Initial commands
addpath('/home/spirals/Documents/MATLAB')
import NL_static_model.*;
%% 
% Function to process dataset and create plots
processDataset = @(file_PATH) processAndPlotDataset(file_PATH);

%% gros_cluster_open loop_signal
% Dataset A
file_PATH = '/home/spirals/europar-96-artifacts/europar-96_fig-3/europar-96_fig-3a.csv';
processDataset(file_PATH);
resultTable = processFunction(inputData)
%% dahu_cluster_open loop_signal
% Dataset B
file_PATH = '/home/spirals/europar-96-artifacts/europar-96_fig-3/europar-96_fig-3b.csv';
processDataset(file_PATH);
%% _cluster_open loop_signal
% Dataset C
file_PATH = '/home/spirals/europar-96-artifacts/europar-96_fig-3/europar-96_fig-3c.csv';
processDataset(file_PATH);
%% processDataset funtion
% Function to process and plot dataset
function processAndPlotDataset(file_PATH)
    % Read the dataset
    data = readtable(file_PATH);
    addpath('/home/spirals/Documents/MATLAB')
    import NL_static_model.*;
    % Convert the 'signal' column to a categorical array
    data.signal = categorical(data.signal);
    t=45:5:120;
    % Create a cell array to store the tables
    signalTables = {};
% Plotting
    % Iterate over unique signal types
    uniqueSignals = unique(data.signal);
    for i = 1:numel(uniqueSignals)
        % Filter the data for the current signal type
        currentSignal = uniqueSignals(i);
        filteredData = data(data.signal == currentSignal, :);

        % Create a table for the current signal type
        signalTable = table();
        signalTable.signal = repmat(currentSignal, size(filteredData, 1), 1);
        signalTable.values = filteredData.value;
        signalTable.runtime = filteredData.runtime;
         t = [0.70393, 45.708, 50.703, 60.701, 70.705, 80.708, 90.702, 100.7]; 

        % Store the signal table in the cell array
        signalTables{i} = signalTable;

        % Plot the values for the current signal type
        if i == 1
            stairs(filteredData.runtime, filteredData.value, 'DisplayName', char(currentSignal));

        elseif i == 3
    
beta = {'gros', 0.83; 'chifflot', 1.03; 'dahu', 0.94};
delta = {'gros', 7.07; 'chifflot', 4.04; 'dahu', 0.17};
alpha = {'gros', 0.047; 'chifflot', 0.028; 'dahu', 0.032};
gamma = {'gros', 28.5; 'chifflot', 37.04; 'dahu', 34.8};
K = {'gros', 25.6; 'chifflot', 42.82; 'dahu', 42.4};

% Asymptotic model of the system 
Clusters = {'gros_model', 'chifflot_model', 'dahu_model'};
y = cell(1, numel(Clusters));
t=45:5:120;
for i = 1:numel(Clusters)
    y{i} = K{i, 2} * (1 - exp(-alpha{i, 2} * (beta{i, 2} * filteredData.runtime - gamma{i, 2} + delta{i, 2})));
end

         e=filteredData.value-y{1};
       
        % plot(fighfgflteredData.runtime,e,'b'); To display the modeling error
        
  %filtering process data to show the nonlinear behaviour of the system. 
        
 
            plot(filteredData.runtime, filteredData.value,'o' ,'DisplayName', char(currentSignal));
        else
            plot(filteredData.runtime, filteredData.value,'o' ,'DisplayName', char(currentSignal));
       
        end
    
    end
% Set the legend
    legend('show');

  % Display the table for selected runtime values
   % Access the tables for each signal type
    for i = 3
        fprintf('Signal Type: %s\n', char(signalTables{i}.signal(1)));
        disp(signalTables{i});
        fprintf('\n');
  
    inputData = signalTables{i} % Define the input data for the function


   

    end
% Use the resultTable as needed
disp(resultTable);
    function processTable = processFunction(inputData)
    % Process the inputData to generate a table
    processTable = table();
    % Perform operations on inputData to generate the table
  end
end
    